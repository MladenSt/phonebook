﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PhoneBook.Abstract;
using PhoneBook.DAL.Abstract;

namespace PhoneBook.DAL
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddPhoneBookDal(
            this IServiceCollection services, Action<DbContextOptionsBuilder> addDbContextOptions)
        {
            services.AddDbContext<PhoneBookDbContext>(addDbContextOptions);
            services.AddTransient<DbContext, PhoneBookDbContext>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IQuery, Query>();
            services.AddTransient<IContactDataProvider, ContactDataProvider>();
            services.AddTransient<IDataProvider, DataProvider>();

            return services;
        }
    }
}
