﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PhoneBook.Models;
using PhoneBook.Services;

namespace PhoneBook
{
    public interface ICustomMapper
    {
        Task<Contact> ToDbModel(ContactAllData c);
    }

    public class CustomMapper : ICustomMapper
    {
        private readonly IMapper _mapper;
        private readonly IAppService _appService;

        public CustomMapper(IMapper mapper, IAppService appService)
        {
            _mapper = mapper;
            _appService = appService;
        }

        public async Task<Contact> ToDbModel(ContactAllData c)
        {
            var contact = _mapper.Map<Contact>(c);
            await IdentifyTags(contact);
            return contact;
        }

        private async Task IdentifyTags(Contact contact)
        {
            var contactTags = contact.Tags.Select(e => e.Tag);
            await _appService.IdentifyTags(contactTags);
            foreach (var contactTag in contact.Tags)
                contactTag.SetTagId();
        }
    }
}
