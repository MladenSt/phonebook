﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using PhoneBook.Services;

namespace PhoneBook
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddPhoneBook(this IServiceCollection services)
        {
            services.AddTransient<IContactsService, ContactsService>();
            services.AddTransient<IAppService, AppService>();
            services.AddTransient<ICustomMapper, CustomMapper>();
            return services;
        }
    }
}
